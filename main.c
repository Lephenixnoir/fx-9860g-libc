/*
	libc test file.
*/

#include "fxlib.h"
#include <stdio.h>
#include <string.h>

void hexa32(unsigned int n, int x, int y)
{
	char ch[11] = "0x";
	int i;

	for(i = 0; i < 8; i++)
	{
		ch[9 - i] = (n & 15) + '0';
		n >>= 4;
	}
	for(i = 2; i < 10; i++) if(ch[i] > '9') ch[i] += 39;
	ch[10] = 0;

	PrintMiniSd(x, y, (const unsigned char *)ch, 0);
}

void write(int x, int y, unsigned int n)
{
	char ch[20];
	sprintf(ch, "%d", n);

	locate(x,y);
	Print((unsigned char *)ch);
}

void print(int x, int y, const char *str)
{
	locate(x,y);
	Print((unsigned char *)str);
}


int main(void)
{
	char *strs[] = { "a376X-", "a376X-", "aaA", "aaD-", "aaX-", "aaA-" };
	unsigned int key;

	Bdisp_AllClr_VRAM();
	write(1, 1, strncmp(strs[0], strs[1], 3));
	write(1, 2, strncmp(strs[2], strs[3], 4));
	write(1, 3, strncmp(strs[4], strs[5], 2));
	GetKey(&key);

	return 1;
}
