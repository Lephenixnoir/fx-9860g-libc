
	.section .pretext

	.global initialize
	.global	_exit

initialize:
	/* Saving the procedure register. */
	sts.l	pr, @-r15

	/* Setting up TLB. */
	mov.l	Hmem_SetMMU, r3
	mov.l	address_one, r4 ! 0x08102000
	mov.l	address_two, r5 ! 0x8801E000
	jsr	@r3
	mov	#108, r6

	/* Clearing the .bss section. */
	mov.l	bbss, r4
	mov.l	ebss, r5
	bra	L_bss_check
	mov	#0, r6
L_bss_clear:
	mov.l	r6, @r4
	add	#4, r4
L_bss_check:
	cmp/hs	r5, r4
	bf	L_bss_clear

	/* Copying romdata to area between bdata and edata. */
	mov.l	bdata, r4
	mov.l	edata, r5
	mov.l	romdata, r6
	bra	L_data_check
	nop
L_data_copy:
	mov.l	@r6+, r3
	mov.l	r3, @r4
	add	#4, r4
L_data_check:
	cmp/hs	r5, r4
	bf	L_data_copy

	/* Computing the size of the bss and data sections.
	mov.l	bbss, r4
	mov.l	edata, r5
	sub	r4, r5
	add	#4, r5
	mov.l	bssdatasize, r4
	mov.l	r5, @r4
	*/

	mov.l	GLibAddinAplExecutionCheck, r2
	mov	#0, r4
	mov	#1, r5
	jsr	@r2 ! _GLibAddinAplExecutionCheck(0,1,1);
	mov	#1, r6

	/* Restoring the procedure register. */
	lds.l	@r15+, pr



save_state:

	/* Saving the current execution state. */
	sts.l	pr, @-r15
	stc.l	sr, @-r15
	stc.l	vbr, @-r15
	mov.l	r8, @-r15
	mov.l	r9, @-r15
	mov.l	r10, @-r15
	mov.l	r11, @-r15
	mov.l	r12, @-r15
	mov.l	r13, @-r15
	mov.l	r14, @-r15

	/* Saving the stack address. */
	mov.l	stack_address, r1
	mov.l	r15, @r1



call_main:
	/* Saving the procedure register. */
	sts.l	pr, @-r15

	/* Calling main function. */
	mov.l	main, r3
	jsr	@r3 ! _main()
	nop

	/* Restoring the procedure register. */
	lds.l	@r15+, pr



_exit:
	/* Restoring the stack address. */
	mov.l	stack_address, r1
	mov.l	@r1, r15

	/* Restoring the execution state. */
	mov.l	@r15+, r14
	mov.l	@r15+, r13
	mov.l	@r15+, r12
	mov.l	@r15+, r11
	mov.l	@r15+, r10
	mov.l	@r15+, r9
	mov.l	@r15+, r8
	ldc.l	@r15+, vbr
	ldc.l	@r15+, sr
	lds.l	@r15+, pr

	/* Calling __atexit, that is atexit() callbacks. */
	sts.l	pr, @-r15
	mov.l	__atexit, r14
	jsr	@r14
	nop
	lds.l	@r15+, pr



_quit:
	/* Saving a few registers. */
	mov.l	r14, @-r15
	mov.l	r13, @-r15
	mov.l	r12, @-r15
	sts.l	pr, @-r15

	mov.l	Bdel_cychdr, r14
	jsr	@r14 ! _Bdel_cychdr
	mov	#6, r4
	jsr	@r14 ! _Bdel_cychdr
	mov	#7, r4
	jsr	@r14 ! _Bdel_cychdr
	mov	#8, r4
	jsr	@r14 ! _Bdel_cychdr
	mov	#9, r4
	jsr	@r14 ! _Bdel_cychdr
	mov	#10, r4
	
	mov.l	BfileFLS_CloseFile, r12
	mov	#4, r14
	mov	#0, r13
L_close_files:
	jsr	@r12 ! _BfileFLS_CloseFile
	mov	r13, r4
	add	#1, r13
	cmp/ge	r14, r13
	bf	L_close_files

	mov.l	flsFindClose, r12
	mov	#0, r13
L_close_finds:
	jsr	@r12 ! _flsFindClose
	mov	r13, r4
	add	#1, r13
	cmp/ge	r14, r13
	bf	L_close_finds

	lds.l	@r15+, pr
	mov.l	@r15+, r12
	mov.l	@r15+, r13
	mov.l	Bkey_Set_RepeatTime_Default, r2
	jmp	@r2 ! _Bkey_Set_RepeatTime_Default
	mov.l	@r15+, r14

	rts
	mov	#1, r0



	.global	_abort

_abort:
	/* Restoring the stack address. */
	mov.l	stack_address, r1
	mov.l	@r1, r15

	/* Restoring the execution state. */
	mov.l	@r15+, r14
	mov.l	@r15+, r13
	mov.l	@r15+, r12
	mov.l	@r15+, r11
	mov.l	@r15+, r10
	mov.l	@r15+, r9
	mov.l	@r15+, r8
	ldc.l	@r15+, vbr
	ldc.l	@r15+, sr
	lds.l	@r15+, pr

	/* Jumping to quit procedure without calling atexit() handlers. */
	mov.l	quit, r1
	jmp	@r1
	nop



	.align 4

address_one:			.long 0x08102000
address_two:			.long 0x8801E000
Hmem_SetMMU:			.long _Hmem_SetMMU
Bdel_cychdr:			.long _Bdel_cychdr
flsFindClose:			.long _flsFindClose
GLibAddinAplExecutionCheck:	.long _GLibAddinAplExecutionCheck
BfileFLS_CloseFile:		.long _BfileFLS_CloseFile
Bkey_Set_RepeatTime_Default:	.long _Bkey_Set_RepeatTime_Default
bbss:				.long _bbss
ebss:				.long _ebss
edata:				.long _edata
bdata:				.long _bdata
romdata:			.long _romdata
bssdatasize:			.long _bssdatasize

main:		.long	_main
__atexit:	.long	___atexit_callbacks
quit:		.long	_quit

clear:		.long	_Bdisp_AllClr_DD

	.comm	_stack_address, 4, 4

stack_address:
	.long	_stack_address

_Hmem_SetMMU:
	mov.l	sc_addr, r2
	mov.l	1f, r0
	jmp	@r2
	nop
1:	.long 0x3FA

_Bdel_cychdr:
	mov.l	sc_addr, r2
	mov.l	1f, r0
	jmp	@r2
	nop
1:	.long 0x119

_BfileFLS_CloseFile:
	mov.l	sc_addr, r2
	mov.l	1f, r0
	jmp	@r2
	nop
1:	.long 0x1E7

_Bkey_Set_RepeatTime_Default:
	mov.l	sc_addr, r2
	mov.l	1f, r0
	jmp	@r2
	nop
1:	.long 0x244

_flsFindClose:
	mov.l	sc_addr, r2
	mov.l	1f, r0
	jmp	@r2
	nop
1:	.long 0x218

_GLibAddinAplExecutionCheck:
	mov.l   sc_addr, r2
	mov     #0x13, r0
	jmp     @r2
	nop

sc_addr:	.long 0x80010070
