#
#	libc Makefile
#

.PHONY: all



#
#	Variables declarations
#

as  =  sh3eb-elf-as
cc  =  sh3eb-elf-gcc
oc  =  sh3eb-elf-objcopy
ar  =  sh3eb-elf-ar
wr  =  g1a-wrapper

flags     =  -m3 -mb -Os -nostdlib -Wall -Wextra -pedantic -std=c99
includes  =  -I. -IincludeFX -isystem include
libs      =  -Llib -lgcc -lc -lfxL

ssrc  =  $(shell find src/ -name '*.s')
sobj  =  $(patsubst %.s, build/%.o, $(ssrc)) build/crt0.o
csrc  =  $(shell find src/ -name '*.c')
cobj  =  $(patsubst %.c, build/%.o, $(csrc))



#
#	Specific rules
#

all: lib/libc.a build/libc.g1a

install:
	usb-connector SEND bin/libc.g1a libc.g1a fls0

lib/libc.a: $(sobj) $(cobj)
	$(ar) rcs lib/libc.a $^
	@ echo "[ \033[32;1mOK \033[0m]  Library created. Output \033[36;1m`stat -c %s lib/libc.a`\033[0m bytes."

build/libc.g1a: build/crt0.o build/main.o lib/libc.a
	$(cc) $(flags) $^ -T bin/addin.ld -o build/libc.elf $(libs)
	$(oc) -R .comment -R .bss -O binary build/libc.elf build/libc.bin
	$(wr) build/libc.bin -o bin/libc.g1a -i bin/icon.bmp
	@ echo "[ \033[32;1mOK\033[0m ]  Build completed. Output \033[36;1m`stat -c %s bin/libc.g1a`\033[0m bytes."



#
#	Generic rules
#

build/%.o: %.c
	$(cc) $(flags) -c $< -o $@ $(includes)

build/%.o: %.s
	$(as) -c $< -o $@

## Specific building instructions

build/crt0.o: bin/crt0.s
	$(as) -c bin/crt0.s -o build/crt0.o

## Clearing

clear:
	@ rm -rf build/*.o
	@ rm -rf build/*/*/*.o
	@ rm -f build/$(NAME).elf
	@ rm -f build/$(NAME).bin

mrproper: clear
	@ rm -f lib/$(NAME).a
	@ rm -f bin/$(NAME).g1a
