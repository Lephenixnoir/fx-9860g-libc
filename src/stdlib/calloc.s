	.global	_calloc
	.type	_calloc, @function

_calloc:
	sts.l	pr, @-r15

	mul.l	r5, r4
	sts	macl, r6
	mov.l	r6, @-r15

	jsr	@r1
	mov.l	malloc, r1

	mov.l	@r15+, r6

	tst	r0, r0
	bt	null

	mov	r0, r4
	mov	#0, r5

	jsr	@r1
	mov.l	memset, r1

	rts
	lds.l	@r15+, pr
 
null:
	rts
	lds.l	@r15+, pr

	.align 4

malloc:
	.long	_malloc
memset:
	.long	_memset
