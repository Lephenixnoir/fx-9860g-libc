	.global	_free
	.type	_free, @function

_free:
	mov.l	sc_addr, r2
	mov.l	sc_id, r0
	jmp	@r2
	nop
 
sc_addr:	.long 0x80010070
sc_id:		.long 0x0acc
