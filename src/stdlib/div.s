
	.global	_div
	.type	_div, @function

_div:
	sts.l	pr, @-r15
	mov.l	r4, @-r15
	mov.l	r5, @-r15

	mov.l	divide, r1
	jsr	@r1
	nop

	mov.l	@r15+, r5
	mov.l	@r15+, r4
	lds.l	@r15+, pr


	mul.l	r5, r0
	sts	macl, r1
	sub	r4, r1

	rts
	neg	r1, r1



	.align 4

divide:
	.long	___sdivsi3
