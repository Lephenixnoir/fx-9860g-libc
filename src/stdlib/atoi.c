#include <stdlib.h>
#include <ctype.h>

int atoi(const char *str)
{
	// Using a final integer and its sign.
	int n = 0;
	int sign = 1;

	// Parsing string until the first glyph.
	while(isspace(*str)) str++;

	// Checking sign.
	if(*str == '-') sign = -1, str++;
	else if(*str == '+') sign = 1, str++;

	// Reading digits.
	while(isdigit(*str))
	{
		n += *str - '0';
		if(*++str) n *= 10;
	}

	// Returning the final value.
	return n * sign;
}
