#include <stdlib.h>
#include <stdio.h>
#include "fxlib.h"

static int atexit_number = 0;
static void (*callbacks[8])(void);

int atexit(void (*callback)(void))
{
	if(atexit_number == 8) return 1;

	callbacks[atexit_number] = callback;
	atexit_number++;

	return 0;
}

void __atexit_callbacks(void)
{
	int i = 0;

	while(i < atexit_number) (*callbacks[i++])();
}
