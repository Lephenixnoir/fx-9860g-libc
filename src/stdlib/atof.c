#include <stdlib.h>
#include <ctype.h>

double atof(const char *str)
{
	// Using a double value and its sign.
	double x = 0.;
	int xsign= 1;
	// Using an exponent and its sign.
	int e = 0;
	int esign = 1;
	// Using a multiplier and an iterator.
	double mul = .1;
	int i;

	// Skipping the first spaces.
	while(isspace(*str)) str++;

	// Looking for a sign on the first character found.
	if(*str == '-') xsign = -1, str++;
	else if(*str == '+') xsign = 1, str++;
	// Returning on illegal input.
	else if(!isdigit(*str) && *str != '.') return (double)(0.);

	// Reading the integer part.
	while(isdigit(*str))
	{
		// Incrementing x.
		x += *str - '0';
		// Multiplying it by 10 until the last glyph is found.
		if(isdigit(*++str)) x *= 10.;
	}

	// Exiting if an illegal character is found.
	if(*str != '.' && tolower(*str) != 'e') return (double)(x*xsign);

	// Reading the decimal part.
	if(*str == '.')
	{
		// Skipping the dot.
		str++;
		// Reading the decimal part.
		while(isdigit(*str))
		{
			// Multiplying the digit by a multiplier and adding it.
			x += mul * (*str - '0');
			// Updating the multiplier to match the next digit.
			mul *= .1;
			// Incrementing str pointer.
			str++;
		}

		// After that, the only character acceptable is an exponent.
		if(tolower(*str) != 'e') return (double)(x*xsign);
	}

	// Incrementing str that was pointing to 'e' or 'E'.
	str++;

	// Checking the exponent sign.
	if(*str == '-') esign = -1, str++;
	else if(*str == '+') esign = 1, str++;
	// Returning on illegal input.
	else if(!isdigit(*str)) return (double)(x*xsign);

	// Reading the exponent value.
	while(isdigit(*str))
	{
		// Updating the integer value.
		e += *str-'0';
		// Multiplying it to match next digit.
		if(isdigit(*++str)) e *= 10;
	}

	// Initializing exponent multiplier.
	mul = 1.;
	// Creating the multiplier value for the exponent.
	for(i = 0; i < e; i++) mul *= 10.;
	// Inverting it to handle negative exponents.
	if(esign == -1) mul = 1 / mul;

	// Returning the final value.
	return (double)(x * xsign * mul);
}
