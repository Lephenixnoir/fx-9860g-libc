
	.comm	_rand_seed, 4, 4

	.global	_srand
	.type	_srand, @function
	.global	_rand
	.type	_rand, @function

_srand:
	mov.l	rand_seed, r1
	rts
	mov.l	r4, @r1

_rand:
	mov.l	rand_seed, r1
	mov.l	@r1, r0

	mov.l	long1, r3
	mul.l	r3, r0
	sts	macl, r0

	mov.l	long2, r3
	add	r3, r0

	rts
	mov.l	r0, @r1

long1:
	.long 1103515245
long2:
	.long 12345

	.align 4

rand_seed:
	.long _rand_seed
