/*
	strncmp()

	Compares two strings with a maximum of n characters.

	r1	First extracted character.
	r2	Second extracted character.
	r4	First string.
	r5	Second string.
	r6	Maximum number of characters to compare.
*/

	.global	_strncmp
	.type	_strncmp, @function

_strncmp:
	/* Getting next characters. */
	mov.b	@r4+, r1
	mov.b	@r5+, r2
	add	#-1, r6

	/* Testing exit conditions. */
	tst	r1, r1
	bt	exit
	tst	r2, r2
	bt	exit
	tst	r6, r6
	bt	exit

	/* Looping if characters are equal. */
	cmp/eq	r1, r2
	bt	_strncmp

	/* Exiting when finished. */
exit:
	mov	r1, r0
	rts
	sub	r2, r0
