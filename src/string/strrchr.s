/*
	strrchr()

	Searches the last occurrence of a byte in a string.

	r0 is initialized as NULL, and whenever an occurrence is found, it is
	updated. When reaching the end of the string, returning does the job.

	r0	Pointer on last occurrence, or NULL if not found.
	r1	Extracted character.
	r4	String pointer.
	r5	Wanted character.
*/

	.global	_strrchr
	.type	_strrchr, @function

_strrchr:
	/* Initializing r0. */
	mov	#0, r0

	/* Main loop. */
loop:

	/* Getting the next character. */
	mov.b	@r4+, r1

	/* Handling string end. */
	tst	r1, r1
	bt	end

	/* Looping if different that r5. */
	cmp/eq	r1, r5
	bf	loop

	/* Updating r0. */
	mov	r4, r0

	/* Looping again. */
	bra	loop
	add	#-1, r0

	/* End of function. */
end:
	rts
	nop
