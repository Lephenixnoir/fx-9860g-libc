/*
	strlen()

	Computes the length of a string.

	r0	String length.
	r1	Extracted character.
	r4	String pointer.
*/

	.global	_strlen
	.type	_strlen, @function

_strlen:
	/* Initializing length. */
	mov	#-1, r0
loop:

	/* Getting the next character. */
	mov.b	@r4+, r1

	/* Looping if not null. */
	add	#1, r0
	tst	r1, r1
	bf	loop

	/* Returning. */
	rts
	nop
