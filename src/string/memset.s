/*
	memset()

	Initializes a memory block width a given byte.

	Memory is set from the end address to the beginning address to take
	advantage of pre-decrement instruction functionalities.
	The function writes single bytes at the beginning and the end and uses
	longword operations in the middle (designed as "4-byte sequence").

	r1	Real end.
	r2	4-byte value to write.
	r3	4-byte sequence beginning (temporarily : mask).
	r4	4-byte sequence end.
	r5	Single byte to write.
	r6	Working pointer.
*/

	.global	_memset
	.type	_memset, @function

_memset:
	/* Initializing working register r6 (real begin). */
	add	r4, r6

	/* Getting real end, 4-byte sequence end and 4-byte sequence begin. */
	mov	r4, r1

	mov.l	mask, r3
	add	#3, r4
	and	r3, r4

	and	r6, r3

	/* Calculating 4-byte value. */
	mov	r5, r2
	shll8	r2
	or	r5, r2
	shll8	r2
	or	r5, r2
	shll8	r2
	or	r5, r2

before:
	/* Writing bytes from real begin to 4-byte sequence begin. */
	cmp/eq	r6, r3
	bt	clear
	mov.b	r5, @-r6
	bra	before
	nop

clear:
	/* Writing 4-aligned from 4-byte seq begin to 4-byte sequence end. */
	mov.l	r2, @-r6
	cmp/eq	r6, r4
	bf	clear

after:
	/* Writing bytes from 4-byte sequence end to real end. */
	cmp/eq	r6, r1
	bt	finish
	mov.b	r5, @-r6
	bra	after
	nop

finish:
	/* Returning from function. */
	rts
	/* Returning real end. */
	mov	r1, r0



	.align 4

mask:
	.long	0xfffffffc
