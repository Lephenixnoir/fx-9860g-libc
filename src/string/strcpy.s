/*
	strcpy()

	Copies a string.

	r0	r4.
	r1	Extracted character.
	r4	Destination.
	r5	Source.
*/

	.global	_strcpy
	.type	_strcpy, @function

_strcpy:
	/* Initializing r0. */
	mov	r4, r0

loop:
	/* Getting next character. */
	mov.b	@r5+, r1

	/* Writing it. */
	mov.b	r1, @r4
	add	#1, r4

	/* Testing the string end. */
	tst	r1, r1
	bf	loop

	/* Returning. */
	rts
	nop
