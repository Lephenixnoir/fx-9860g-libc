/*
	strchr()

	Searches the first occurrence byte in a string.

	r0	Pointer on first occurrence, or NULL if not found.
	r1	Extracted character.
	r4	String pointer.
	r5	Wanted character.
*/

	.global	_strchr
	.type	_strchr, @function

_strchr:
	/* Getting the next character. */
	mov.b	@r4+, r1

	/* Handling string end. */
	tst	r1, r1
	bt	null

	/* Seeking the wanted character. */
	cmp/eq	r1, r5
	bf	_strchr
	mov	r4, r0
	rts
	add	#-1, r0

	/* If not found, returning NULL. */
null:
	rts
	mov	#0, r0
