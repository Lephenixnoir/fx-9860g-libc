/*
	strcmp()

	Compares two strings.

	r1	First extracted character.
	r2	Second extracted character.
	r4	First string.
	r5	Second string.
*/

	.global	_strcmp
	.type	_strcmp, @function

_strcmp:
	/* Getting next characters. */
	mov.b	@r4+, r1
	mov.b	@r5+, r2

	/* Testing exit conditions. */
	tst	r1, r1
	bt	end
	tst	r2, r2
	bt	end
	cmp/eq	r1, r2
	bt	_strcmp

end:
	/* Coputing the result. */
	sub 	r2, r1
	rts
	mov	r1, r0
