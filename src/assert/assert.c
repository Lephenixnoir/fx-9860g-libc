#include <assert.h>
#include <stdlib.h>
#include "fxlib.h"

void __assert_fail(const char *expr, const char *file, unsigned int line)
{
	unsigned int key;
	int length = 0;
	Bdisp_AllClr_VRAM();

	while(expr[length]) length++;

	PrintXY(6, 1, (const unsigned char *)"Assertion failed !!", 0);
	Bdisp_AreaReverseVRAM(0, 0, 127, 8);

	locate(1, 3);
	Print((unsigned char *)"`");
	locate(2, 3);
	Print((unsigned char *)expr);
	locate(2 + length, 3);
	Print((unsigned char *)"`");

	locate(3, 5);
	Print((unsigned char *)"in");
	locate(6, 5);
	Print((unsigned char *)file);
	locate(1, 6);
	Print((unsigned char *)"line");
	locate(6, 6);
	Print((unsigned char *)"23");

	locate(1, 8);
	Print((unsigned char *)"Aborting execution.");

	GetKey(&key);
	abort();
}
