/*
	Header inclusions.
*/

// Defining __NEED___BASE_PRINTF defines prototypes for __base_printf() and
// external declaration for the buffer (meant for internal purposes).
#define __NEED___BASE_PRINTF
#include <stdio.h>
// Using atoi() function.
#include <stdlib.h>
// Using various string functions, mainly strlen() and strchr().
#include <string.h>
// Using variable argument functionalities, obviously.
#include <stdarg.h>
// Using isdigit() function and assimilated.
#include <ctype.h>



/*
	Internal buffer.

	Using a buffer *really* simplifies everything. But it also has
	disadvantages, such a memory loss and limited output size.

	So, in case we find a possibility to get rid of this buffer, we will
	just have to change function character(), which is for now the only
	function that directly interface with the buffer.
*/

// This macro is defined in stdio.h, but not when compiling with standard
// headers.
#ifdef __MAIN
#define __base_printf_buffer_size 256
#endif // __MAIN

// Defining the buffer data.
char __base_printf_buffer[__base_printf_buffer_size];



/*
	Format composed types.

	Format structure handles everything in a format, from data type to
	given value, including alternative forms, alignment and digit numbers.

	A format is made of a data type, which can be altered by a size option,
	a number of integer and decimal digits, and additional flags.

	The FormatFlags enumeration handles the various flags that can be added
	to a printf()-family format.
*/

enum FormatFlags
{
	// Option '#' specifies alternatives forms, mainly '0' and '0x'
	// prefixes in integer display.
	Alternative	= 1,
	// Under specific conditions, zero-padding may replace padding
	// whitespaces by zeros.
	ZeroPadded	= 2,
	// Left alignment specifies that additional spaces should be added
	// after the value.
	LeftAlign	= 4,
	// In numeric display, this forces a blank sign to be written before
	// positive values.
	BlankSign	= 8,
	// In numeric display, this forces an explicit sign in all cases. This
	// option overrides BlankSign (see __base_printf() description for
	// further information on option precedence and influence).
	ForceSign	= 16
};

struct Format
{
	// Format type, one of 'diouxXcs' ('eEfFgGaApnm' still to add).
	char type;
	// Format size, one of 'l', 'h', 'i' ('i' means 'hh').
	char size;

	// Number of characters printed.
	int characters;
	// Number of digits after the dot.
	int precision;

	// Various flags.
	enum FormatFlags flags;

	// Value to output.
	union	
	{
		// Using an signed int with formats %c, %d and %i.
		signed int _int;
		// Using an unsigned int with formats %o, %u, %x and %X.
		unsigned int _unsigned;
		// Using a double with formats %f, %F, %e, %E, %g and %G.
		double _double;
		// Using a string pointer with format %s.
		const char *_pointer;
	};
};



/*
	Static functions declaration.
*/

// Outputs a character in the buffer. Updates counters.
static void character(int c);
// Outputs n timers the given character.
static void character_n(int c, int n);
// Reads a format from the given string pointer address (must begin with '%').
static struct Format get_format(const char **pointer);
// Computes the number of spaces and zeros to add to the bare output.
static void get_spacing(struct Format format, int *begin_spaces, int *sign,
	int *zeros, int length, int *end_spaces);

// Outputs an signed int to the  buffer.
static void __printf_itoa(struct Format format);
// Outputs an unsigned int to the  buffer.
static void __printf_utoa(struct Format format);
// Outputs a double in exponent notation in the buffer.
static void __printf_etoa(struct Format format);
// Outputs a character to the buffer.
static void __printf_ctoa(struct Format format);
// Outputs a string to the  buffer.
static void __printf_stoa(struct Format format);



/*
	Additional macros.
*/

#define abs(x) ((x) < 0 ? -(x) : (x))



/*
	Static variables definitions.
*/

// Number of characters currently written.
static size_t written = 0;
// Total number of function calls (characters theoretically written).
static size_t total = 0;
// Maximum number of characters to output.
static size_t max = 0;

/*
	character()

	Outputs a character to the buffer. This function centralizes all the
	buffer interface, so that if we come to remove it for property reasons,
	we would just have to edit this function.

	Static variables written and total are both needed, because the
	terminating NUL character may be written after the maximum has been
	reached.
	In other words, when the function ends, we need to have a variable
	counting the current position in the buffer (written), and one other
	containing the total number of theoretic characters (total) because
	these two values may be different.

	Of course the three variables need to be initialized before using this
	function.

	@arg	c	Character to output.
*/

static void character(int c)
{
	// If there is space left in the buffer.
	if(written < max - 1)
	{
		// Writing the character in the buffer.
		__base_printf_buffer[written] = c;
		// Increasing the written counter.
		written++;
	}

	// Always increasing the number of calls.
	total++;
}

/*
	character_n()

	Outputs n times the same character. Thought to be used to output spaces
	or zeros without heavy annoying iterative loops.
*/

static void character_n(int c, int n)
{
	int i = 0;
	while(i++ < n) character(c);
}

/*
	get_format()

	Reads the format from the given string pointer and returns a
	corresponding Format structure. The string pointer points to is also
	modified, so that is points to the first character after the format.

	@arg	pointer		Address of the format string.

	@return		Format structure describing the represented format.
*/

static struct Format get_format(const char **pointer)
{
	// Using a list of conversion specifiers.
	const char *convspec = "diouxXeEfFgGaAcspnm";
	// Using a format structure.
	struct Format format;
	// Using a simpler parsing pointer.
	const char *string = *pointer;
	// Using a common pointer.
	const char *ptr;
	// Using an integer to retrieve a character from the format string,
	// and an iterator.
	int c, i;
	// Using a string to store precision informations.
	char precision[10];

	// Moving the string pointer after the '%' character.
	string++;

	// Initializing structure type and size.
	format.type = format.size = 0;
	// Initializing digit counts.
	format.characters = format.precision = -1;
	// Initializing flags.
	format.flags = 0;

	// Parsing the format string. Testing each character until a
	// conversion specifier is found.
	while((c = *string))
	{
		// Looking for a conversion specifier.
		ptr = strchr(convspec, c);
		if(ptr)
		{
			// Setting the format attribute.
			format.type = *ptr;
			// Breaking the analysis : format has ended.
			break;
		}

		// Looking for a left precision string (number of digits before
		// the dot), introduced by a non-null digit.
		if(c >= '1' && c <= '9')
		{
			// Parsing the left precision.
			for(i = 0; i < 9 && *string >= '0' && *string <= '9';
				string++)
			{
				// Getting next character.
				precision[i++] = *string;
			}

			// Adding a NUL terminating character.
			precision[i] = 0;
			// Setting the structure attribute.
			format.characters = atoi(precision);

			// As pointer is now pointing on the next character,
			// continuing to handle it correctly.
			continue;
		}

		// Looking for a right precision string (number of digits after
		// the dot), introduced by a point.
		if(c == '.')
		{
			// Skipping the dot.
			string++;

			// Just checking that the next character is a digit.
			if(!isdigit(*string)) continue;

			// Parsing the right precision.
			for(i = 0; i < 9 && *string >= '0' && *string <= '9';
				string++)
			{
				// Getting next character.
				precision[i++] = *string;
			}

			// Adding a NUL terminating character.
			precision[i] = 0;
			// Setting the structure attribute.
			format.precision = atoi(precision);

			// As pointer is now pointing on the next character,
			// continuing to handle it correctly.
			continue;
		}

		// Handling predefined characters.
		switch(*string)
		{
			// Handling length modifiers.
		case 'h':
			format.size = 'h' + (format.size == 'h');
			break;
		case 'l':
		case 'L':
		case 'z':
		case 't':
			format.size = *string;
			break;

			// Handling flags.
		case '#':
			format.flags |= Alternative;
			break;
		case '0':
			format.flags |= ZeroPadded;
			break;
		case '-':
			format.flags |= LeftAlign;
			break;
		case ' ':
			format.flags |= BlankSign;
			break;
		case '+':
			format.flags |= ForceSign;
			break;
		}

		// Increasing the format pointer.
		string++;
	}

	// If the format isn't ended, the type attribute is left to zero and
	// the main loop will handle failure and break. Nothing has to be done
	// here.

	// Setting the string pointer, that now points to the conversion
	// specifier.
	*pointer = string + 1;

	// Returning the parsed format structure.
	return format;
}

/*
	get_spacing()

	Computes the arrangement of beginning spaces, sign, zeros, pure value
	and ending spaces in formats.
	This formatting follows a recurrent model which is centralized in this
	function.

	@arg	format		Format to compute for.
	@arg	begin_spaces	Will be set to the number of spaces to add
				before the zeros.
	@arg	sign		Sign character.
	@arg	zeros		Will be set to the number of zeros to add
				before the pure output.
	@arg	length		Length of pure output.
	@arg	end_spaces	Will be set to the number of spaces to add
				after the pure output (exclusive with
				begin_spaces ; at least one is zero).
*/

static void get_spacing(struct Format format, int *begin_spaces, int *sign,
	int *zeros, int length, int *end_spaces)
{
	// Using a list of types involving a sign.
	const char *signed_types = "dieEfFgGaA";
	// Using an integer to store the number of spaces to add.
	int spaces;
	// Digits represents pure output + zeros (don't mix up with the *real*
	// displayed digits).
	int digits;
	// Saving the left-aligned property of the format.
	int left = format.flags & LeftAlign;

	// Getting the total number of digits.
	switch(format.type)
	{
	// In integer display, the number of digits output is specified in the
	// precision.
	case 'd':
	case 'i':
		// Using the format precision.
		digits = format.precision;
		// If invalid, falling back to the pure length.
		if(digits < length) digits = length;
		break;

	case 'o':
	case 'u':
	case 'x':
	case 'X':
		// Using the format precision.
		digits = format.precision;
		// Adding prefixes to digits.
		if(format.flags & Alternative)
		{
			// Using an integer to store hexadecimal format status.
			int hexa = (format.type == 'x' || format.type == 'X');
			// Adding a common '0' and an 'x' or 'X' in base 16.
			digits += 1 + hexa;
			// Also incrementing length not to get wrong results
			// when adding a prefix (eventually this is the same
			// as having reduced the character number : and in
			// fact, that's what should be done when adding
			// prefixes).
			length += 1 + hexa;
		}
		break;

	// Other formats do not have additional zeros.
	default:
		digits = length;
		break;
	}


	if(sign)
	{
		// Getting the sign. Falling back to NUL (no sign).
		*sign = 0;
		// Only testing signed format types.
		if(strchr(signed_types, format.type))
		{
			// Using a blank sign if specified.
			if(format.flags & BlankSign) *sign = ' ';
			// Option '+' overrides option ' '.
			if(format.flags & ForceSign) *sign = '+';
			// And of course negative sign overrides everything !
			if(format.type == 'd' || format.type == 'i')
			{
				if(format._int < 0) *sign = '-';
			}
			else if(format._double < 0) *sign = '-';

		}
	}

	// Computing the number of spaces.
	spaces = format.characters - digits;
	// Computing the number of zeros.
	*zeros = digits - length;

	// Removing a space when a sign appears.
	if(sign && *sign) spaces--;

	// Option '0' translates spaces to zeros, but only if no precision is
	// specified ; additionally, left alignment overrides zero-padding.
	if(!left && format.precision == -1 && format.flags & ZeroPadded)
	{
		// Translating spaces to zeros.
		*zeros += spaces;
		// Removing all the spaces.
		spaces = 0;
	}

	// First setting both spaces number to zero.
	*begin_spaces = 0;
	*end_spaces = 0;
	// Then setting the correct one to the computed value, depending on the
	// left alignment specification.
	if(left) *end_spaces = spaces;
	else *begin_spaces = spaces;
}

/*
	__base_printf()

	Basic buffered formatted printing function. Full-featured, so that
	every call to a printf()-family function can be translated to a
	__base_printf() call.

	It always returns the number of characters of the theoretic formatted
	output. The real output may be limited in size by the given size
	argument when working with nprintf()-family functions, or the internal
	buffer itself.

	The Flags structure isn't necessary, but it simplifies a lot format
	argument handling (because flag effects depend on format type, which
	is unknown when the flags are read). Also, length modifiers 'hh' is
	stored as 'i' to simplify structure handling. 'll' is not supported.
	Format '%LF' is allowed by C99 (therefore supported), but not by SUSv2.

	Generic information on options precedence and influence.
	-	Influences of integer part and mantissa digit numbers depend on
		the type of data that is being displayed.
	-	Option '#' doesn't contend with any other.
	-	Option '+' overrides options ' '.
	-	In integer display, option '0' translates spaces to zeros, but
		only if no decimal digit number is specified.
		The option '-' also overrides it, forcing whitespaces to be
		written at the end of the format.

	Limit of function.
	-	Internal buffer size (must bu customizable with a -D option
		when compiling).
	-	Precision values (format %a.b) are written on 8 bits, therefore
		limited to 127.

	Unsupported features.
	-	Flag character ''' (single quote) for thousands grouping
	-	Flag character 'I', that outputs locale's digits (glibc 2.2)
	-	Length modifiers 'll' and 'q' (libc 5 and 4.4 BSD)
	-	This is not really a feature but incorrect characters in
		formats are ignored and don't invalidate the format.

	@arg	size	Maximum number of bytes that will be written to the
			buffer.
	@arg	format	Format string.
	@arg	args	Additional arguments, as a va_list object.

	@return		Number of bytes of the theoretic formatted output.
*/

int __base_printf(size_t size, const char *string, va_list args)
{
	/*
		Variables declaration.
	*/

	// Using a format structure.
	struct Format format;



	/*
		Initialization.
	*/

	// Avoiding overflow. Setting default size if not limited by the call.
	if(!size || size > __base_printf_buffer_size)
	{
		// Using the maximal size supported by the buffer.
		size = __base_printf_buffer_size;
	}

	// Initializing character() working values.
	written = total = 0;
	max = size;



	/*
		Format string parsing.
	*/

	// Parsing each character. At each iteration, a literal character, a
	// '%%' identifier or a format is parsed.
	while(*string)
	{
		// Handling literal characters.
		if(*string != '%') character(*string++);

		// Handling literal '%'.
		else if(string[1] == '%')
		{
			string += 2;
			character('%');
		}

		// Handling complete format.
		else
		{
			// Getting the format.
			format = get_format(&string);
			// Breaking the loop if it's non-ended.
			if(!format.type) break;

/*
			// Displaying an information message.
			printf(
				"Format found :%s%c%c, options %d, and %d.%d "
				"digits\n",
				format.size ? " " : "",
				format.size ? format.size : ' ',
				format.type,
				format.flags,
				format.digits,
				format.mantissa
			);
*/

			// Getting the value as the right type and calling the
			// corresponding subroutine.
			switch(format.type)
			{
			// Handling signed integers.
			case 'd':
			case 'i':
				// Getting the argument as signed int.
				format._int = va_arg(args, signed int);

				// Reducing value depending on format size.
				switch(format.size)
				{
					// Handling shorts.
				case 'h':
					format._int &= 0x0000ffff;
					break;
					// Handling characters.
				case 'i':
					format._int &= 0x000000ff;
					break;
				}

				// Calling the complex itoa() routine.
				__printf_itoa(format);

				break;

			// Handling unsigned integers.
			case 'o':
			case 'u':
			case 'x':
			case 'X':
				// Getting the argument as  unsigned int.
				format._unsigned = va_arg(args, unsigned int);
				// Calling the corresponding routine.
				__printf_utoa(format);

				break;

			// Handling exponent notation.
			case 'e':
			case 'E':
				// Getting the argument.
				format._double = va_arg(args, double);
				// Calling the exponent notation routine.
				__printf_etoa(format);

				break;

			// Handling characters.
			case 'c':
				// Getting the argument.
				format._int = va_arg(args, signed int);
				// Reducing it to a char.
				format._int &= 0xff;

				// Calling the appropriated routine.
				__printf_ctoa(format);

				break;

			// Handling strings.
			case 's':
				// Getting the argument.
				format._pointer = va_arg(args, const char *);
				// Calling the string routine.
				__printf_stoa(format);

				break;
			}
		}
	}

	// Adding a terminating NUL character. Function character() should have
	// left an empty byte for that.
	__base_printf_buffer[written] = 0;

	// Returning the total number of characters.
	return total;
}

/*
	__printf_itoa()

	Subroutine itoa(). Writes the given signed integer to the internal
	buffer, trough function character().
	It is used by conversion specifiers 'd' and 'i'.
	Options '#' and '0' have no actual effect.

	@arg	format		Format to write. It includes the value.
*/

static void __printf_itoa(struct Format format)
{
	// In integer display, character number includes pure digits and
	// additional zeros and spacing.
	// The precision represents the number of digits (pure digits and
	// zeros) to print.
	// For example: ' 0004', pure digits: 1, digits: 4, characters: 5.

	/*
		Variable declarations.
	*/

	// Using a character for the number sign.
	int sign = 0;
	// Using an integer to store the value to output.
	signed int x = format._int;
	// Using integers to store the number pure digits and additional spaces
	// and zeros.
	int bspaces, zeros, digits = 0, espaces;
	// Using a multiplier to output digit in the correct order.
	int multiplier = 1;



	// Returning if the argument is null with an explicit precision of
	// zero, but only if there are no spaces.
	if(!x && format.characters == -1 && !format.precision) return;




	/*
		Computations.
	*/

	// Computing the number of digits.
	x = abs(format._int);
	// If x is null, output only one digit ('0').
	if(!x) digits = 1;
	// Otherwise, looping until there's no more digits. Also computing the
	// multiplier.
	else while(x)
	{
		// Increasing the number of digits.
		digits++;
		// Dividing x.
		x /= 10;
		// Updating the multiplier (condition prevents an unwanted
		// extra operation).
		if(x) multiplier *= 10;
	}

	// Getting the corresponding spacing.
	get_spacing(format, &bspaces, &sign, &zeros, digits, &espaces);



	/*
		Output.
	*/

	// Writing the beginning whitespaces.
	character_n(' ', bspaces);
	// Writing the sign if existing.
	if(sign) character(sign);
	// Writing the zeros.
	character_n('0', zeros);

	// Initializing x;
	x = abs(format._int);
	// Writing the pure digits, except if the value is null with an
	// explicit precision of zero.
	if(x || format.precision) while(multiplier)
	{
		// Extracting the next character.
		character((x / multiplier) % 10 + '0');
		// Dividing the multiplier.
		multiplier /= 10;
	}

	// Writing ending whitespaces if left-aligned.
	character_n(' ', espaces);
}

/*
	__printf_utoa()

	Subroutine utoa(). Writes the given unsigned int in base 8, 10 or 16
	depending on the specified conversion.
	Since the argument is unsigned, options ' ' and '+' have no effect.
	Option '#' adds prefix '0' in octal or '0x' in hexadecimal.

	@arg	format	Format to write. It includes the value.
*/

static void __printf_utoa(struct Format format)
{
	// In unsigned display, the digit number specifies the minimal number
	// of characters that should be output. If the prefix (alternative
	// form) is present, it is part of this count.
	// Integer part and decimal part digit numbers behave the same way as
	// in signed integer display.

	// Using an integer to store the working base.
	int base = 10;
	// Using integers to store the number of digits, zeros and spaces.
	int bspaces, zeros, digits = 0, espaces;
	// Using a value to copy displayed one.
	int x = format._unsigned;
	// Using a multiplier to extract the digits one by one.
	int multiplier = 1;
	// Using an integer to store a character.
	int c;



	/*
		Computations.
	*/

	// Getting the base.
	switch(format.type)
	{
	// Handling octal base.
	case 'o':
		base = 8;
		break;
	// Handling decimal base.
	case 'u':
		base = 10;
		break;
	// Handling hexadecimal base.
	case 'x':
	case 'X':
		base = 16;
		break;
	}

	// Using default value if x is null.
	if(!x) digits = 1;
	// Computing the number of digits.
	else while(x)
	{	
		// Increasing the number of digits.
		digits++;
		// Dividing x.
		x /= base;
		// Updating the multiplier (without a last useless operation).
		if(x) multiplier *= base;
	}

	// Getting the spaces and zeros repartition.
	get_spacing(format, &bspaces, NULL, &zeros, digits, &espaces);



	/*
		Display.
	*/

	// Writing the beginning whitespaces.
	character_n(' ', bspaces);

	// Getting the format value to display.
	x = format._unsigned;

	// Writing the alternative form prefix.
	if(format.flags & Alternative && x)
	{
		// Writing a common prefix character '0'.
		character('0');
		// Adding 'x' or 'X' in hexadecimal format.
		if(format.type == 'x' || format.type == 'X')
		{
			// Directly writing the format character.
			character(format.type);
		}
	}

	// Writing the zeros.
	character_n('0', zeros);

	// Extracting the digits.
	while(multiplier)
	{
		// Extracting the next character.
		c = (x / multiplier) % base;
		// Handling uppercase and lowercase.
		c += '0' + (c > 9) * (7 + 32 * (format.type == 'x'));

		// Writing the character.
		character(c);
		// Dividing the multiplier.
		multiplier /= base;
	}

	// Writing the ending whitespaces.
	character_n(' ', espaces);
}

/*
	__printf_etoa()

	Subroutine stoa(). Writes the given double argument in exponent
	notation.
	Option '#' has no effect.

	@arg	format	Format to write. It includes the value.
*/

static void __printf_etoa(struct Format format)
{
	// In exponent display, the precision is the number of digits after the
	// dot.

	// Using an integer to store the number exponent.
	int exponent = 0;
	// Using a double value for temporary computations, and another to
	// store the format parameter.
	double tmp = 1, x = format._double;
	// Using spacing variables. Default length is for '0.e+00';
	int bspaces, zeros, sign, length = 6, espaces;
	// Using an iterator and a multiplier.
	int i, mul;



	/*
		Computations.
	*/

	// Computing the exponent. For positive exponents, increasing until
	// the temporary value gets greater than x.
	if(x > 1)
	{
		// Looping until we haven't reached a greater exponent.
		while(tmp < x)
		{
			// Incrementing the exponent.
			exponent++;
			// Multiplying the test value.
			tmp *= 10;
		}
		// Removing an additional incrementation.
		exponent--;
	}
	// For negative exponents, decreasing until it's lower.
	else while(tmp > x)
	{
		// Decrementing the exponent.
		exponent--;
		// Dividing the test value.
		tmp *= 0.1;
	}

	// Adding a character if the exponent is greater that 100.
	if(exponent >= 100) length++;
	// Adding another one if it's greater than 1000.
	if(exponent >= 1000) length++;

	// Adjusting the format precision, defaulting to 6.
	if(format.precision == -1) format.precision = 6;
	// Adding the decimal digits.
	length += format.precision;

	// Getting the space repartition.
	get_spacing(format, &bspaces, &sign, &zeros, length, &espaces);



	/*
		Output.
	*/

	// Writing the beginning whitespaces.
	character_n(' ', bspaces);
	// Writing the sign if existing.
	if(sign) character(sign);
	// Writing the zeros.
	character_n('0', zeros);

	// Initializing x.
	x = abs(format._double) / tmp;
	// Writing the first digit.
	character(x + '0');
	character('.');

	// Writing the decimal digits.
	for(i = 0; i < format.precision; i++)
	{
		// Multiplying x by 10 and getting rid of the previous digit.
		x = (x - (int)x) * 10;
		// Writing the current digit.
		character(x + '0');
	}

	// Writing the exponent letter and its sign.
	character(format.type);
	character(exponent < 0 ? '-' : '+');

	// Getting a positive exponent.
	exponent = abs(exponent);

	// Using a multiplier for the exponent.
	if(exponent >= 1000) mul = 1000;
	else if(exponent >= 100) mul = 100;
	else mul = 10;

	// Writing the exponent characters.
	while(mul)
	{
		// Writing the next character.
		character((exponent / mul) % 10 + '0');
		// Dividing the multiplier.
		mul *= 0.1;
	}

	// Writing the ending whitespaces if left-aligned.
	character_n(' ', espaces);
}

/*
	__printf_ctoa()

	Subroutine ctoa(). Writes the given character. Actually only handles
	left alignment and spacing.
	Options '#', '0', ' ' and '+', as well as mantissa digit number, have
	no actual effect.

	@arg	format	Format to write. It includes the value.
*/

static void __printf_ctoa(struct Format format)
{
	// In character display, the digit number represents the number of
	// characters written, including the argument and additional
	// whitespaces.

	// Using an integer to store the number of whitespaces.
	int spaces = format.characters - 1;
	// Using an integer to check if the format is left-aligned.
	int left = format.flags & LeftAlign;

	// If not left-aligned, outputting the spaces.
	if(!left) character_n(' ', spaces);

	// Writing the character.
	character(format._int & 0xff);

	// If left-aligned, outputting the spaces.
	if(left) character_n(' ', spaces);
}

/*
	__printf_stoa()

	Subroutine stoa() (which doesn't make any sense, but it had to be named
	so just because the others were named that way).
	Writes the given string, adding spacing if needed.

	@arg	format	Format to write. It includes the pointer.
*/

void __printf_stoa(struct Format format)
{
	// In string display, the character number specify the minimum size of
	// output (padded with whitespaces if needed) and the precision
	// specify the maximum number of string characters output.

	// Using an integer to store the number of written string characters.
	int string = format.precision;
	// Using an integer to store the number of whitespaces.
	int spaces;
	// Using a string pointer.
	const char *str = format._pointer;
	// Using the string length and an iterator.
	int length, i;
	// Using an integer to store the format left alignment.
	int left = format.flags & LeftAlign;

	// Getting the string length.
	length = strlen(str);
	// Computing the real number of characters.
	if(string > length || string == -1) string = length;
	// Computing the number of whitespaces.
	spaces = format.characters - string;

	// If not left-aligned, writing the whitespaces.
	if(!left) character_n(' ', spaces);
	// Writing the characters.
	for(i = 0; i < string; i++) character(str[i]);
	// If left-aligned, writing the whitespaces.
	if(left) character_n(' ', spaces);
}



/*
	Testing options.

	Includes a simple printf() implementation that uses fputs() and a main
	function that compares output of the standard printf() and the custom
	defined here.

	Activate macro __MAIN by adding '-D __MAIN' to command arguments in
	order to use these functions.
*/

#ifdef __MAIN

int _printf(const char *format, ...)
{
	va_list args;
	int x;

	va_start(args, format);
	x = __base_printf(0, format, args);
	fputs(__base_printf_buffer, stdout);
	va_end(args);

	return x;
}

int main(void)
{
	const char *format = "'% 20.4e'\n";
	double value = 1.0684e-5;

	_printf(format, value);
	printf(format, value);

	return 0;
}

#endif // _MAIN
