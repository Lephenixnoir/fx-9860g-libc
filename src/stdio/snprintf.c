#define __NEED___BASE_PRINTF
#include <stdio.h>

int snprintf(char *str, size_t size, const char *format, ...)
{
	// Variables declaration.
	int x, n=0;
	va_list args;

	// Calling __sprintf() to perform formatting.
	va_start(args,format);
	x = __base_printf(size,format,args);

	while(__base_printf_buffer[n] && (size_t)n<size-1) str[n] = __base_printf_buffer[n], n++;
	str[n] = 0;

	va_end(args);
	return x;
}
