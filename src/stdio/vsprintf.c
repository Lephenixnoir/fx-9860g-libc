#define __NEED___BASE_PRINTF
#include <stdio.h>

int vsprintf(char *str, const char *format, va_list args)
{
	// Variables declaration.
	int x, n=0;

	// Calling __sprintf() to perform formatting.
	x = __base_printf(0,format,args);

	while(__base_printf_buffer[n]) str[n] = __base_printf_buffer[n], n++;
	str[n] = 0;

	return x;
}
