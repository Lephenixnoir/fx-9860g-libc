#define __NEED___BASE_PRINTF
#include <stdio.h>

int vsnprintf(char *str, size_t size, const char *format, va_list args)
{
	// Variables declaration.
	int x, n=0;

	// Calling __sprintf() to perform formatting.
	x = __base_printf(size,format,args);

	while(__base_printf_buffer[n] && (size_t)n<size-1) str[n] = __base_printf_buffer[n], n++;
	str[n] = 0;

	return x;
}
