#ifndef _CTYPE_H
	#define _CTYPE_H 1

#ifdef __cplusplus
extern "C" {
#endif

// Character definition macros.
#define isalnum(c) (isdigit(c) || isalpha(c))
#define isalpha(c) (islower(c) || isupper(c))
#define iscntrl(c) ((c)<=0x1F || (c)==0x7F)
#define isdigit(c) ((c)>=0x30 && (c)<=0x39)
#define isgraph(c) ((c)>=0x21 && (c)<=0x7E)
#define islower(c) ((c)>=0x61 && (c)<=0x7A)
#define isprint(c) ((c)>=0x20 && (c)<=0x7E)
#define ispunct(c) (((c)>=0x21 && (c)<=0x2F) || ((c)>=0x3A && (c)<=0x40) || ((c)>=0x5B && \
	(c)<=0x60) || ((c)>=0x7B && (c)<=0x7E))
#define isspace(c) (((c)>=0x09 && (c)<=0x0D) || (c)==0x20)
#define isupper(c) ((c)>=0x41 && (c)<=0x5A)
#define isxdigit(c) (((c)>=0x30 && (c)<=0x39) || ((c)>=0x41 && (c)<=0x46) || ((c)>=0x61 \
	&& (c)<=0x66))

// Character manipulation macros.
#define tolower(c) (isupper(c) ? (c)|32 : (c))
#define toupper(c) (islower(c) ? (c)&~32 : (c))

#ifdef __cplusplus
}
#endif

#endif // _CTYPE_H
