#ifndef _STDINT_H
	#define _STDINT_H 1

// intmax_t type definition.
#ifndef _INTMAX_T
	#define _INTMAX_T 1
	typedef signed int intmax_t;
	typedef unsigned int uintmax_t;
#endif // _INTMAX_T

#define INTMAX_MIN	INT_MIN
#define INTMAX_MAX	INT_MAX
#define UINTMAX_MAX	UINT_MAX

// Type definitions.
#ifndef _INTN_T
	#define _INTN_T 1
	typedef signed char int8_t;
	typedef unsigned char uint8_t;
	typedef signed short int16_t;
	typedef unsigned short uint16_t;
	typedef signed int int32_t;
	typedef unsigned int uint32_t;
#endif // _INTN_T

#define INT8_MIN	SCHAR_MIN
#define INT8_MAX	SCHAR_MAX
#define UINT8_MAX	UCHAR_MAX
#define INT16_MIN	SHRT_MIN
#define INT16_MAX	SHRT_MAX
#define UINT16_MAX	USHRT_MAX
#define INT32_MIN	INT_MIN
#define INT32_MAX	INT_MAX
#define UINT32_MAX	UINT_MAX

// intptr_t type definition.
#ifndef _INTPTR_T
	#define _INTPTR_T 1
	typedef signed int intptr_t;
	typedef unsigned int uintptr_t;
#endif // _INTPTR_T

#define INTPTR_MIN	INT_MIN
#define INTPTR_MAX	INT_MAX
#define UINTPTR_MAX	UINT_MAX

#endif // _STDINT_H
