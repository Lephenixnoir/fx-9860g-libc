#ifndef _STDARG_H
	#define _STDARG_H 1

#ifdef __cplusplus
extern "C" {
#endif

// va_list type definition.
typedef __builtin_va_list va_list;

// Macro definition of va_list-related functions.
#define va_start(list,argument) __builtin_va_start(list,argument)
#define va_arg(list,type) __builtin_va_arg(list,type)
#define va_end(list) __builtin_va_end(list)
#define va_copy(dest,src) __builtin_va_copy(dest,src)

#ifdef __cplusplus
}
#endif

#endif // _STDARG_H
