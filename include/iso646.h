#ifndef _ISO646_H
	#define _ISO646_H 1

// These macros do not have any sense in C++ since the keywords exist.
#ifndef __cplusplus

// Operators aliases declaration.
#define and		&&
#define and_eq		&=
#define bitand		&
#define bitor		|
#define compl		~
#define not		!
#define not_eq		!=
#define or		||
#define or_eq		|=
#define xor		^
#define xor_eq		^=

#endif // __cplusplus

#endif // _ISO646_H
