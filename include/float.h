#ifndef _FLOAT_H
	#define _FLOAT_H 1

#ifdef __cplusplus
extern "C" {
#endif

// Radix, Mantissa and Digits parameters.
#define FLT_RADIX		2
#define FLT_MANT_DIG		24
#define DBL_MANT_DIG		53
#define FLT_DIG			6
#define DBL_DIG			15

// Limits for floating-point values.
#define FLT_MIN_EXP		-125
#define DBL_MIN_EXP		-1021
#define FLT_MAX_EXP		128
#define DBL_MAX_EXP		1024
#define FLT_MAX			__FLT_MAX__
#define DBL_MAX			__DBL_MAX__
#define FLT_EPSILON		__FLT_EPSILON__
#define DBL_EPSILON		__DBL_EPSILON__
#define FLT_MIN			__FLT_MIN__
#define DBL_MIN			__DBL_MIN__

// Other related parameters.
#define FLT_ROUNDS		1
#define FLT_EVAL_METHOD		0
#define DECIMAL_DIGITS		17

#ifdef __cplusplus
}
#endif

#endif // _FLOAT_H
