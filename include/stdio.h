#ifndef _STDIO_H
	#define _STDIO_H 1

#ifdef __cplusplus
extern "C" {
#endif

// stdio.h should not define va_list.
#include <stdarg.h>

// size_t type definition.
#ifndef _SIZE_T
	#define _SIZE_T 1
	typedef unsigned int size_t;
#endif // _SIZE_T

// NULL pointer definition.
#ifndef _NULL
	#define _NULL 1
	#define NULL ((void *)0L)
#endif // _NULL

// Formatted printing functions.
int sprintf(char *str, const char *format, ...);
int snprintf(char *str, size_t size, const char *format, ...);
int vsnprintf(char *str, size_t size, const char *format, va_list args);
int vsprintf(char *str, const char *format, va_list args);

// Stuff used by *printf() functions source files.
#ifdef __NEED___BASE_PRINTF
	#define __base_printf_buffer_size 256
	extern char __base_printf_buffer[];
	int __base_printf(size_t size, const char *format, va_list args);
#endif // __NEED___BASE_PRINTF

#ifdef __cplusplus
}
#endif

#endif // _STDIO_H
