#ifndef _STDLIB_H
	#define _STDLIB_H

#ifdef __cplusplus
extern "C" {
#endif

// NULL pointer definition.
#ifndef _NULL
	#define _NULL 1
	#define NULL ((void *)0L)
#endif // _NULL

// div_t type definition.
#ifndef _DIV_T
	#define _DIV_T 1
	typedef struct
	{
		int quot;
		int rem;
	} div_t;
#endif // _DIV_T

// size_t type definition.
#ifndef _SIZE_T
	#define _SIZE_T 1
	typedef unsigned int size_t;
#endif // _SIZE_T

// Usual macros. Only defined by this file.
#define EXIT_FAILURE	0
#define EXIT_SUCCESS	1
#define RAND_MAX	2147483647

// Conversion functions.
int atoi(const char *str);
double atof(const char *str);

// Random numbers generation.
int rand(void);
void srand(unsigned int seed);

// Dynamic allocation functions.
void free(void *ptr);
void *malloc(size_t size);
void *calloc(size_t num, size_t size);
void *realloc(void *ptr, size_t size);

// Numeric functions.
int abs(int n);
div_t div(int numerator, int denumerator);

// Environment functions.
int atexit(void (*callback)(void));
void abort(void);
void exit(int status);

#ifdef __cplusplus
}
#endif

#endif // _STDLIB_H
