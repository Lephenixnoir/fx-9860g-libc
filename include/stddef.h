#ifndef _STDDEF_H
	#define _STDDEF_H 1

#ifdef __cplusplus
extern "C" {
#endif

// ptrdiff_t type definition.
#ifndef _PTRDIFF_T
	#define _PTRDIFF_T 1
	typedef signed int ptrdiff_t;
#endif // _PTRDIFF_T

// size_t type definition.
#ifndef _SIZE_T
	#define _SIZE_T 1
	typedef unsigned int size_t;
#endif // _SIZE_T

// ssize_t type definition.
#ifndef _SSIZE_T
	#define _SSIZE_T 1
	typedef signed int ssize_t;
#endif // _SSIZE_T

// offsetof() macro declaration.
#ifndef _OFFSETOF
	#define _OFFSETOF 1
	#define offsetof(type,member) ((size_t)&((type *)0->member))
#endif

// NULL pointer definition.
#ifndef _NULL
	#define _NULL 1
	#define NULL ((void *)0L)
#endif // _NULL

#ifdef __cplusplus
}
#endif

#endif // _STDDEF_H
