#ifndef _STRING_H
	#define _STRING_H

#ifdef __cplusplus
extern "C" {
#endif

/*
	Type definitions.
*/

#ifndef _SIZE_T
	#define _SIZE_T 1
	typedef unsigned int size_t;
#endif // _SIZE_T



/*
	Function prototypes.
*/

void *memset(void *pointer, int value, size_t length);

char *strcpy(char *string, const char *source);

int strcmp(const void *string_1, const void *string_2);
int strncmp(const void *string_1, const void *string_2, size_t length);

const char *strchr(const char *string, int value);
char *strrchr(const char *string, int value);

size_t strlen (const char *str);

#ifdef __cplusplus
}
#endif

#endif // _STRING_H
