#ifndef _ASSERT_H
	#define _ASSERT_H

// assert() doesn't do anything if NDEBUG is defined.
#ifdef NDEBUG
// Removing call.
#define assert(expr) ((void)0)

// Otherwise, defining the macro.
#else
#define assert(expr) ((expr) \
	? ((void)0) \
	: __assert_fail(#expr, __FILE__, __LINE__))
#endif // NDEBUG

void __assert_fail(const char *expr, const char *file, unsigned int line);

#endif // _ASSERT_H
