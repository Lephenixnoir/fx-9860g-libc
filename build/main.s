	.file	"main.c"
	.text
	.text
	.align 1
	.global	_hexa32
	.type	_hexa32, @function
_hexa32:
	mov.l	r14,@-r15
	sts.l	pr,@-r15
	add	#-28,r15
	mov	r15,r14
	mov	r14,r1
	add	#-36,r1
	mov.l	r4,@(44,r1)
	mov	r14,r1
	add	#-36,r1
	mov.l	r5,@(40,r1)
	mov	r14,r1
	add	#-36,r1
	mov.l	r6,@(36,r1)
	mov	r14,r1
	add	#12,r1
	mov.l	.L7,r2
	mov.l	r2,@r1
	add	#4,r1
	mov	#0,r2
	mov.l	r2,@r1
	mov	#0,r2
	mov	r2,r0
	mov.w	r0,@(4,r1)
	mov	#0,r2
	mov	r2,r0
	mov.b	r0,@(6,r1)
	mov	r14,r1
	add	#-36,r1
	mov	#0,r2
	mov.l	r2,@(60,r1)
	bra	.L2
	nop
	.align 1
.L3:
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r1
	neg	r1,r1
	add	#9,r1
	mov	r14,r2
	add	#-36,r2
	mov.l	@(44,r2),r2
	extu.b	r2,r2
	mov	#15,r3
	and	r3,r2
	extu.b	r2,r2
	add	#48,r2
	extu.b	r2,r2
	exts.b	r2,r3
	mov	r14,r2
	add	#12,r2
	mov	r1,r0
	mov.b	r3,@(r0,r2)
	mov	r14,r1
	add	#-36,r1
	mov	r14,r2
	add	#-36,r2
	mov.l	@(44,r2),r2
	shlr2	r2
	shlr2	r2
	mov.l	r2,@(44,r1)
	mov	r14,r1
	add	#-36,r1
	mov	r14,r2
	add	#-36,r2
	mov.l	@(60,r2),r2
	add	#1,r2
	mov.l	r2,@(60,r1)
.L2:
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r2
	mov	#7,r1
	cmp/gt	r1,r2
	bf	.L3
	mov	r14,r1
	add	#-36,r1
	mov	#2,r2
	mov.l	r2,@(60,r1)
	bra	.L4
	nop
	.align 1
.L6:
	mov	r14,r2
	add	#12,r2
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r1
	add	r2,r1
	mov.b	@r1,r2
	mov	#57,r1
	cmp/gt	r1,r2
	bf	.L5
	mov	r14,r2
	add	#12,r2
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r1
	add	r2,r1
	mov.b	@r1,r1
	extu.b	r1,r1
	add	#39,r1
	extu.b	r1,r1
	exts.b	r1,r2
	mov	r14,r3
	add	#12,r3
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r1
	add	r3,r1
	mov.b	r2,@r1
.L5:
	mov	r14,r1
	add	#-36,r1
	mov	r14,r2
	add	#-36,r2
	mov.l	@(60,r2),r2
	add	#1,r2
	mov.l	r2,@(60,r1)
.L4:
	mov	r14,r1
	add	#-36,r1
	mov.l	@(60,r1),r2
	mov	#9,r1
	cmp/gt	r1,r2
	bf	.L6
	mov	r14,r1
	add	#12,r1
	mov	#0,r2
	mov	r2,r0
	mov.b	r0,@(10,r1)
	mov	r14,r3
	add	#12,r3
	mov	r14,r2
	add	#-36,r2
	mov	r14,r1
	add	#-36,r1
	mov	#0,r7
	mov	r3,r6
	mov.l	@(36,r2),r5
	mov.l	@(40,r1),r4
	mov.l	.L8,r1
	jsr	@r1
	nop
	nop
	add	#28,r14
	mov	r14,r15
	lds.l	@r15+,pr
	mov.l	@r15+,r14
	rts	
	nop
.L9:
	.align 2
.L7:
	.long	813170688
.L8:
	.long	_PrintMiniSd
	.size	_hexa32, .-_hexa32
	.section	.rodata
	.align 2
.LC0:
	.string	"%d"
	.text
	.align 1
	.global	_write
	.type	_write, @function
_write:
	mov.l	r14,@-r15
	sts.l	pr,@-r15
	add	#-32,r15
	mov	r15,r14
	mov	r14,r1
	add	#-32,r1
	mov.l	r4,@(40,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	r5,@(36,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	r6,@(32,r1)
	mov	r14,r1
	add	#-32,r1
	mov.l	.L11,r3
	mov	r14,r2
	add	#12,r2
	mov.l	@(32,r1),r6
	mov	r3,r5
	mov	r2,r4
	mov.l	.L12,r1
	jsr	@r1
	nop
	mov	r14,r2
	add	#-32,r2
	mov	r14,r1
	add	#-32,r1
	mov.l	@(36,r2),r5
	mov.l	@(40,r1),r4
	mov.l	.L13,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#12,r1
	mov	r1,r4
	mov.l	.L14,r1
	jsr	@r1
	nop
	nop
	add	#32,r14
	mov	r14,r15
	lds.l	@r15+,pr
	mov.l	@r15+,r14
	rts	
	nop
.L15:
	.align 2
.L11:
	.long	.LC0
.L12:
	.long	_sprintf
.L13:
	.long	_locate_OS
.L14:
	.long	_Print
	.size	_write, .-_write
	.align 1
	.global	_print
	.type	_print, @function
_print:
	mov.l	r14,@-r15
	sts.l	pr,@-r15
	add	#-12,r15
	mov	r15,r14
	mov	r14,r1
	add	#-52,r1
	mov.l	r4,@(60,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r5,@(56,r1)
	mov	r14,r1
	add	#-52,r1
	mov.l	r6,@(52,r1)
	mov	r14,r2
	add	#-52,r2
	mov	r14,r1
	add	#-52,r1
	mov.l	@(56,r2),r5
	mov.l	@(60,r1),r4
	mov.l	.L17,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#-52,r1
	mov.l	@(52,r1),r4
	mov.l	.L18,r1
	jsr	@r1
	nop
	nop
	add	#12,r14
	mov	r14,r15
	lds.l	@r15+,pr
	mov.l	@r15+,r14
	rts	
	nop
.L19:
	.align 2
.L17:
	.long	_locate_OS
.L18:
	.long	_Print
	.size	_print, .-_print
	.align 1
	.global	_main
	.type	_main, @function
_main:
	mov.l	r14,@-r15
	sts.l	pr,@-r15
	add	#-12,r15
	mov	r15,r14
	mov	r14,r1
	add	#4,r1
	mov.l	.L22,r2
	mov.l	r2,@r1
	mov.l	.L23,r2
	mov.l	r2,@(4,r1)
	mov	r14,r1
	add	#4,r1
	mov.l	.L24,r2
	mov.l	r2,@r1
	mov	#0,r2
	mov	r2,r0
	mov.b	r0,@(4,r1)
	mov.l	.L25,r1
	jsr	@r1
	nop
	mov	r14,r1
	add	#4,r1
	mov	#55,r5
	mov	r1,r4
	mov.l	.L26,r1
	jsr	@r1
	nop
	mov	r0,r1
	mov	r1,r6
	mov	#1,r5
	mov	#1,r4
	mov.l	.L27,r1
	jsr	@r1
	nop
	mov	r14,r1
	mov	r1,r4
	mov.l	.L28,r1
	jsr	@r1
	nop
	mov	#1,r1
	mov	r1,r0
	add	#12,r14
	mov	r14,r15
	lds.l	@r15+,pr
	mov.l	@r15+,r14
	rts	
	nop
.L29:
	.align 2
.L22:
	.long	1768515945
.L23:
	.long	929642345
.L24:
	.long	1952805748
.L25:
	.long	_Bdisp_AllClr_VRAM
.L26:
	.long	_strchr
.L27:
	.long	_print
.L28:
	.long	_GetKey
	.size	_main, .-_main
	.ident	"GCC: (GNU) 5.1.0"
