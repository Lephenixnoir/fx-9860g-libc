# 1 "main.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "main.c"
# 9 "main.c"
# 1 "includeFX/fxlib.h" 1
# 18 "includeFX/fxlib.h"
# 1 "includeFX/dispbios.h" 1
# 65 "includeFX/dispbios.h"
typedef struct tag_DISPBOX{
    int left;
    int top;
    int right;
    int bottom;
} DISPBOX;

typedef struct tag_GRAPHDATA{
    int width;
    int height;
    unsigned char *pBitmap;
} GRAPHDATA;

typedef struct tag_RECTANGLE{
    DISPBOX LineArea;
    unsigned char AreaKind;
    unsigned char EffectWin;
} RECTANGLE;

typedef struct tag_DISPGRAPH{
    int x;
    int y;
    GRAPHDATA GraphData;
    unsigned char WriteModify;
    unsigned char WriteKind;
} DISPGRAPH;
# 19 "includeFX/fxlib.h" 2
# 1 "includeFX/filebios.h" 1
# 32 "includeFX/filebios.h"
enum DEVICE_TYPE{
    DEVICE_MAIN_MEMORY,
    DEVICE_STORAGE,
    DEVICE_SD_CARD,
};
# 101 "includeFX/filebios.h"
typedef struct tag_FILE_INFO
{
    unsigned short id;
    unsigned short type;
    unsigned long fsize;
    unsigned long dsize;
    unsigned int property;
    unsigned long address;
} FILE_INFO;
# 20 "includeFX/fxlib.h" 2
# 1 "includeFX/keybios.h" 1
# 21 "includeFX/fxlib.h" 2




void Bdisp_AllClr_DD(void);
void Bdisp_AllClr_VRAM(void);
void Bdisp_AllClr_DDVRAM(void);
void Bdisp_AreaClr_DD(const DISPBOX *pArea);
void Bdisp_AreaClr_VRAM(const DISPBOX *pArea);
void Bdisp_AreaClr_DDVRAM(const DISPBOX *pArea);
void Bdisp_AreaReverseVRAM(int x1, int y1, int x2, int y2);
void Bdisp_GetDisp_DD(unsigned char *pData);
void Bdisp_GetDisp_VRAM(unsigned char *pData);
void Bdisp_PutDisp_DD(void);
void Bdisp_PutDispArea_DD(const DISPBOX *PutDispArea);
void Bdisp_SetPoint_DD(int x, int y, unsigned char point);
void Bdisp_SetPoint_VRAM(int x, int y, unsigned char point);
void Bdisp_SetPoint_DDVRAM(int x, int y, unsigned char point);
int Bdisp_GetPoint_VRAM(int x, int y);
void Bdisp_WriteGraph_DD(const DISPGRAPH *WriteGraph);
void Bdisp_WriteGraph_VRAM(const DISPGRAPH *WriteGraph);
void Bdisp_WriteGraph_DDVRAM(const DISPGRAPH *WriteGraph);
void Bdisp_ReadArea_DD(const DISPBOX *ReadArea, unsigned char *ReadData);
void Bdisp_ReadArea_VRAM(const DISPBOX *ReadArea, unsigned char *ReadData);
void Bdisp_DrawLineVRAM(int x1, int y1, int x2, int y2);
void Bdisp_ClearLineVRAM(int x1, int y1, int x2, int y2);

void locate(int x, int y);
void Print(const unsigned char *str);
void PrintRev(const unsigned char *str);
void PrintC(const unsigned char *c);
void PrintRevC(const unsigned char *str);
void PrintLine(const unsigned char *str, int max);
void PrintRLine(const unsigned char *str, int max);
void PrintXY(int x, int y, const unsigned char *str, int type);
int PrintMini(int x, int y, const unsigned char *str, int type);
int PrintMiniSd(int x, int y, const unsigned char *str, int type);
void SaveDisp(unsigned char num);
void RestoreDisp(unsigned char num);
void PopUpWin(int n);

int Bfile_OpenFile(const unsigned short *filename, int mode);
int Bfile_OpenMainMemory(const unsigned char *name);
int Bfile_ReadFile(int HANDLE, void *buf, int size, int readpos);
int Bfile_WriteFile(int HANDLE, const void *buf, int size);
int Bfile_SeekFile(int HANDLE, int pos);
int Bfile_CloseFile(int HANDLE);
int Bfile_GetMediaFree(enum DEVICE_TYPE devicetype, int *freebytes);
int Bfile_GetFileSize(int HANDLE);
int Bfile_CreateFile(const unsigned short *filename, int size);
int Bfile_CreateDirectory(const unsigned short *pathname);
int Bfile_CreateMainMemory(const unsigned char *name);
int Bfile_RenameMainMemory(const unsigned char *oldname, const unsigned char *newname);
int Bfile_DeleteFile(const unsigned short *filename);
int Bfile_DeleteDirectory(const unsigned short *pathname);
int Bfile_DeleteMainMemory(const unsigned char *name);
int Bfile_FindFirst(const unsigned short *pathname, int *FindHandle, unsigned short *foundfile, FILE_INFO *fileinfo);
int Bfile_FindNext(int FindHandle, unsigned short *foundfile, FILE_INFO *fileinfo);
int Bfile_FindClose(int FindHandle);

void Bkey_Set_RepeatTime(long FirstCount, long NextCount);
void Bkey_Get_RepeatTime(long *FirstCount, long *NextCount);
void Bkey_Set_RepeatTime_Default(void);
int GetKeyWait(int sel, int time, int menu, unsigned int *keycode);
int IsKeyDown(int keycode);
int IsKeyUp(int keycode);
int GetKey(unsigned int *keycode);

int SetTimer(int ID, int elapse, void (*hander)(void));
int KillTimer(int ID);
void Sleep(int millisecond);

void SetQuitHandler(void (*callback)(void));
int INIT_ADDIN_APPLICATION(int isAppli, unsigned short OptionNum);
# 10 "main.c" 2

# 1 "include/stdio.h" 1 3 4
# 9 "include/stdio.h" 3 4
# 1 "include/stdarg.h" 1 3 4
# 9 "include/stdarg.h" 3 4

# 9 "include/stdarg.h" 3 4
typedef __builtin_va_list va_list;
# 10 "include/stdio.h" 2 3 4




 typedef unsigned int size_t;
# 24 "include/stdio.h" 3 4
int sprintf(char *str, const char *format, ...);
int snprintf(char *str, size_t size, const char *format, ...);
int vsnprintf(char *str, size_t size, const char *format, va_list args);
int vsprintf(char *str, const char *format, va_list args);
# 12 "main.c" 2
# 1 "include/stdlib.h" 1 3 4
# 17 "include/stdlib.h" 3 4
 typedef struct
 {
  int quot;
  int rem;
 } div_t;
# 36 "include/stdlib.h" 3 4
int atoi(const char *str);
double atof(const char *str);


int rand(void);
void srand(unsigned int seed);


void free(void *ptr);
void *malloc(size_t size);
void *calloc(size_t num, size_t size);
void *realloc(void *ptr, size_t size);


int abs(int n);


int atexit(void (*callback)(void));
void abort(void);
void exit(int status);
# 13 "main.c" 2

# 1 "include/assert.h" 1 3 4
# 14 "include/assert.h" 3 4
void __assert_fail(const char *expr, const char *file, unsigned int line);
# 15 "main.c" 2


# 16 "main.c"
void hexa32(unsigned int n, int x, int y)
{
 char ch[11] = "0x";
 int i;

 for(i = 0; i < 8; i++)
 {
  ch[9 - i] = (n & 15) + '0';
  n >>= 4;
 }
 for(i = 2; i < 10; i++) if(ch[i] > '9') ch[i] += 39;
 ch[10] = 0;

 PrintMiniSd(x, y, (const unsigned char *)ch, 0);
}

void quit(void)
{
 Bdisp_AllClr_DD();
 Sleep(1000);
}

int main(void)
{
 atexit(quit);

 
# 42 "main.c" 3 4
0
# 42 "main.c"
              ;
 return 1;
}
