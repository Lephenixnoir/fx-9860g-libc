	.file	"atexit.c"
	.text
	.text
	.align 1
	.align 2
	.global	_atexit
	.type	_atexit, @function
_atexit:
	mov.l	.L5,r1
	mov.l	@r1,r0
	cmp/eq	#8,r0
	bt	.L3
	add	#1,r0
	mov.l	r0,@r1
	rts	
	mov	#0,r0
	.align 1
.L3:
	rts	
	mov	#1,r0
.L6:
	.align 2
.L5:
	.long	_atexit_number
	.size	_atexit, .-_atexit
	.align 1
	.align 2
	.global	___atexit_callbacks
	.type	___atexit_callbacks, @function
___atexit_callbacks:
	rts	
	nop
	.size	___atexit_callbacks, .-___atexit_callbacks
	.local	_atexit_number
	.comm	_atexit_number,4,4
	.ident	"GCC: (GNU) 5.1.0"
